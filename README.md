> Roadmap to becoming a GIS Developer in 2018

## 🚀 1. Introduction

![](https://i.imgur.com/qnhvY69.png)

## 🎨 2. Collecting data

![](https://i.imgur.com/4NSp6K6.png)

## 👷 3. Database

![](https://i.imgur.com/OlyaMCn.png)

## 👷 4. Processing Data
![](https://i.imgur.com/GBVdNsd.png)

## 🚦 5. Spatial Analysis

![](https://i.imgur.com/xywo6MM.png)

## 🚦 6. Programming GIS

![](https://i.imgur.com/Dprlcav.png)

## 🚦 7. Server GIS

![](https://i.imgur.com/RFNkGoX.png)

